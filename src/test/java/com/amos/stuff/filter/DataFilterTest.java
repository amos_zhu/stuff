package com.amos.stuff.filter;

import com.amos.stuff.StuffApplication;
import com.amos.stuff.filter.bean.BizDTO;
import com.amos.stuff.filter.bean.FilterRule;
import com.amos.stuff.filter.handler.DataFilterHandler;
import com.amos.stuff.filter.mock.MockData;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: DataFilterTest
 * @Package: com.amos.stuff.filter
 * @author: zhuqb
 * @Description:
 * @date: 2019/10/9 0009 下午 17:53
 * @Version: V1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {StuffApplication.class})// 指定启动类
@Slf4j
public class DataFilterTest {
    @Test
    public void testFilter() {
        MockData data = new MockData();
        BizDTO dto = data.mockBiz();
        List<FilterRule> filterList = data.mockRule();

        DataFilterHandler handler = new DataFilterHandler(dto, filterList);
        handler.filter();
    }
}
