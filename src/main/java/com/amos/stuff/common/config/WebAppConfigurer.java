package com.amos.stuff.common.config;

import com.amos.stuff.common.service.EnumConverterFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: WebAppConfigurer
 * @Package: com.amos.stuff.common.handler
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/27 0027 上午 10:49
 * @Version: V1.0
 */
@Configuration
public class WebAppConfigurer implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new EnumConverterFactory());
    }
}
