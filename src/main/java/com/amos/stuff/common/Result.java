package com.amos.stuff.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: Result
 * @Package: com.amos.stuff.common
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/26 0026 上午 11:54
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
public class Result {
    /**
     * 编码
     */
    private Integer code;
    /**
     * 信息
     */
    private String msg;
    /**
     * 数据
     */
    private Object data;

}
