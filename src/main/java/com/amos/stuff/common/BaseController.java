package com.amos.stuff.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BaseController
 * @Package: com.amos.stuff.common
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/27 0027 下午 13:46
 * @Version: V1.0
 */
public class BaseController {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

}
