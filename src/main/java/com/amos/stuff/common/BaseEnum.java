package com.amos.stuff.common;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BaseEnum
 * @Package: com.amos.stuff.common
 * @author: zhuqb
 * @Description: 定义所以枚举的公共接口
 * 返回枚举的key值
 * @date: 2019/9/27 0027 上午 10:23
 * @Version: V1.0
 */
public interface BaseEnum {

    String getKey();
}
