package com.amos.stuff.common.type;

import lombok.AllArgsConstructor;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: ResultEnum
 * @Package: com.amos.stuff.common.type
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/26 0026 上午 11:58
 * @Version: V1.0
 */

@AllArgsConstructor
public enum ResultEnum {

    SUCCESS(1, "ok"),
    FAIL(0, "校验错误"),
    ERROR(-1, "系统错误");
    private Integer code;
    private String desc;

    public Integer getCode() {
        return this.code;
    }

    public String getDesc() {
        return this.desc;
    }

    public static Integer success() {
        return SUCCESS.getCode();
    }

    public static Integer fail() {
        return FAIL.getCode();
    }

    public static Integer error() {
        return ERROR.getCode();
    }
}
