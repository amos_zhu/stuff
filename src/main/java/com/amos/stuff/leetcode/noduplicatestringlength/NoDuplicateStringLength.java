package com.amos.stuff.leetcode.noduplicatestringlength;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.stuff.leetcode.noduplicatestringlength
 * @ClassName NoDuplicateStringLength
 * @Description 无重复字符串的最长子串
 * **************************************************************
 * 题目描述：
 * 给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
 * <p>
 * 备注要求：
 *
 * <p>
 * 示例说明：
 * 输入: "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * <p>
 * 分析：
 * <p>
 * ***************************************************************
 * @Author Amos
 * @Modifier
 * @Date 2019/10/28 21:08
 * @Version 1.0
 **/
public class NoDuplicateStringLength {
    private final static Logger logger = LoggerFactory.getLogger(NoDuplicateStringLength.class);

    public static int lengthOfLongestSubstring(String target) {

        if (StringUtils.isEmpty(target)) {
            return 0;
        }

        char[] targetArr = target.toCharArray();

        logger.info("char数组:{}", JSONObject.toJSONString(targetArr));
        int maxSize = 0;
        int[] dest = new int[256];
        int base = 0;
        int index = 0;
        for (int i = 0, len = targetArr.length; i < len; i++) {
            index = targetArr[i];
            if (dest[index] > base) {
                base = dest[index];
            }
            dest[index] = i + 1;
            maxSize = (maxSize > i - base + 1) ? maxSize : i - base + 1;
        }
        return maxSize;
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcabcach"));
    }
}
