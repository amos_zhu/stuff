package com.amos.stuff.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: EnumFindHelper
 * @Package: com.amos.stuff.util
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/27 0027 上午 10:27
 * @Version: V1.0
 */
public class EnumFindHelper<T extends Enum<T>, K> {
    /**
     * 默认enumMap的容量大小
     */
    public static final Integer DEFAULT_MAP_SIZE = 8;
    /**
     * 枚举缓存
     */
    private Map<K, T> enumMap = new HashMap<K, T>(DEFAULT_MAP_SIZE);

    /**
     * 构造器中循环遍历所有的枚举项，然后保存在枚举缓存中
     *
     * @param clazz
     * @param keyGetter
     */
    public EnumFindHelper(Class<T> clazz, EnumKeyGetter<T, K> keyGetter) {
        try {

            for (T enumValue : EnumSet.allOf(clazz)) {
                this.enumMap.put(keyGetter.getKey(enumValue), enumValue);
            }
        } catch (Exception e) {

        }
    }

    /**
     * 通过key来查询枚举对象
     * 用户返回需要校验是否空对象
     *
     * @param key 枚举的key
     * @return
     */
    public T find(K key) {
        T value = this.enumMap.get(key);
        if (value == null) {
            return null;
        }
        return value;
    }

    /**
     * 定义接口获取枚举的Key
     *
     * @param <T> 枚举
     * @param <K> 枚举的Key
     */
    public static interface EnumKeyGetter<T extends Enum<T>, K> {
        /**
         * 通过枚举获取枚举的Key
         *
         * @param enumValue 枚举的对象
         * @return
         */
        K getKey(T enumValue);

    }
}
