package com.amos.stuff.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: SpringContext
 * @Package: com.amos.stuff.util
 * @author: zhuqb
 * @Description: 获取Spring的应用上下文
 * @date: 2019/9/26 0026 下午 13:59
 * @Version: V1.0
 */
@Component
public class SpringContext implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContext.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return SpringContext.applicationContext;
    }

    public static <T> T getBean(Class<T> clazz) {
        return SpringContext.getApplicationContext().getBean(clazz);
    }
}
