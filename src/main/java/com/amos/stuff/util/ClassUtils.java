package com.amos.stuff.util;

import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: ClassUtils
 * @Package: com.amos.stuff.util
 * @author: zhuqb
 * @Description:
 * @date: 2019/10/9 0009 下午 17:40
 * @Version: V1.0
 */
public class ClassUtils {

    /**
     * 获取属性中的List集合类型中的泛型的Class
     *
     * @param field
     * @return
     */
    public static Class getFieldType(Field field) {
        Type type = field.getGenericType();

        if (StringUtils.isEmpty(type)) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            Class clazz = (Class) pt.getActualTypeArguments()[0];
            return clazz;
        }

        return null;

    }
}
