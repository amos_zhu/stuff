package com.amos.stuff.filter.bean;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BaseDTO
 * @Package: com.amos.stuff.filter.bean
 * @author: zhuqb
 * @Description: 数据模型的基类 这里作为泛型约束
 * @date: 2019/10/9 0009 下午 17:27
 * @Version: V1.0
 */
public class BaseDTO {
}
