package com.amos.stuff.filter.bean.entity;

import lombok.Data;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BizEntity
 * @Package: com.amos.stuff.filter.bean.entity
 * @author: zhuqb
 * @Description:
 * @date: 2019/10/10 0010 上午 8:09
 * @Version: V1.0
 */
@Data
public class BizEntity {
    private String name;
    private String email;
    private Integer age;
    private String grade;
}
