package com.amos.stuff.filter.bean;

import com.amos.stuff.filter.anno.AmMapping;
import com.amos.stuff.filter.bean.entity.BizDetailEntity;
import com.amos.stuff.filter.bean.entity.BizEntity;
import lombok.Data;

import java.util.List;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BizDTO
 * @Package: com.amos.stuff.filter.bean
 * @author: zhuqb
 * @Description:
 * @date: 2019/10/9 0009 下午 17:58
 * @Version: V1.0
 */
@Data
@AmMapping(entity = BizEntity.class)
public class BizDTO extends BaseDTO {

    private String name;
    private String email;
    private Integer age;
    private String grade;
    @AmMapping(entity = BizDetailEntity.class)
    private List<BizDetailDTO> bizDetails;
}
