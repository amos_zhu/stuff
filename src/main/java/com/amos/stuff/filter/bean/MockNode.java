package com.amos.stuff.filter.bean;

import lombok.Data;

import java.util.List;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: UnicomMockNode
 * @Package: com.amos.stuff.filter.bean
 * @author: zhuqb
 * @Description: 定义模型结构
 * @date: 2019/10/9 0009 下午 17:32
 * @Version: V1.0
 */
@Data
public class MockNode<T extends BaseDTO> {
    /**
     * 父节点的Node
     */
    private MockNode pre;
    /**
     * 所以子节点的Node
     */
    private List<MockNode> nexts;
    /**
     * 当前节点的 class 类名
     */
    private Class<?> clazz;
    /**
     * 当前节点 不需要显示的 field 全称
     */
    private List<String> noShowFields;
}
