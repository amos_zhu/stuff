package com.amos.stuff.filter.bean;

import lombok.Data;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: FilterRule
 * @Package: com.amos.stuff.filter.bean
 * @author: zhuqb
 * @Description: 过滤规则的对象
 * @date: 2019/10/9 0009 下午 17:23
 * @Version: V1.0
 */
@Data
public class FilterRule {
    /**
     * Entity对象属性全路径
     */
    private String entityFieldFullName;
    /**
     * 是否显示 true 是 false 否
     */
    private Boolean show;
}
