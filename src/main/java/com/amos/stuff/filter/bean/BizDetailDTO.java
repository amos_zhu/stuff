package com.amos.stuff.filter.bean;

import com.amos.stuff.filter.anno.AmMapping;
import com.amos.stuff.filter.bean.entity.BizDetailEntity;
import lombok.Data;

import java.util.Date;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BizDetailDTO
 * @Package: com.amos.stuff.filter.bean
 * @author: zhuqb
 * @Description:
 * @date: 2019/10/9 0009 下午 18:00
 * @Version: V1.0
 */
@Data
@AmMapping(entity = BizDetailEntity.class)
public class BizDetailDTO extends BaseDTO {

    private String company;

    private Date startTime;

    private Integer quantum;

    private String experience;
}
