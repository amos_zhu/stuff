package com.amos.stuff.filter.bean.entity;

import lombok.Data;

import java.util.Date;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BizDetailEntity
 * @Package: com.amos.stuff.filter.bean.entity
 * @author: zhuqb
 * @Description:
 * @date: 2019/10/10 0010 上午 8:13
 * @Version: V1.0
 */
@Data
public class BizDetailEntity {
    private String company;

    private Date startTime;

    private Integer quantum;

    private String experience;
}
