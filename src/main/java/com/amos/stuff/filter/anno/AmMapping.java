package com.amos.stuff.filter.anno;

import java.lang.annotation.*;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: AmMapping
 * @Package: com.amos.stuff.filter.anno
 * @author: zhuqb
 * @Description: DTO映射Entity对象
 * <p>
 * 该属性添加在类或者属性上，通过该注解可以知道DTO对应的Entity对象
 * @date: 2019/10/9 0009 下午 17:18
 * @Version: V1.0
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AmMapping {
    /**
     * 当前DTO对象对应的Entity对象Class信息
     *
     * @return
     */
    Class<?> entity();
}
