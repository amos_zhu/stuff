package com.amos.stuff.filter.mock;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.amos.stuff.filter.bean.BizDTO;
import com.amos.stuff.filter.bean.FilterRule;

import java.util.List;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: MockData
 * @Package: com.amos.stuff.filter.mock
 * @author: zhuqb
 * @Description: 组装模拟数据
 * @date: 2019/10/10 0010 上午 8:16
 * @Version: V1.0
 */
public class MockData {
    /**
     * 模拟DTO数据对象
     *
     * @return
     */
    public BizDTO mockBiz() {
        String mockStr = "{\n" +
                "  \"name\": \"amos\",\n" +
                "  \"email\": \"amoszhu@aliyun.com\",\n" +
                "  \"age\": 30,\n" +
                "  \"grade\": \"6\",\n" +
                "  \"bizDetails\": [\n" +
                "    {\n" +
                "      \"company\": \"xxx公司1\",\n" +
                "      \"startTime\": 1570666889755,\n" +
                "      \"quantum\": 2,\n" +
                "      \"experience\": \"能吃苦耐劳\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"company\": \"xxx公司2\",\n" +
                "      \"startTime\": 1570666899755,\n" +
                "      \"quantum\": 2,\n" +
                "      \"experience\": \"能吃苦耐劳\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        BizDTO bizDTO = JSONObject.parseObject(mockStr, BizDTO.class);
        return bizDTO;
    }

    /**
     * 模拟数据过滤规则
     *
     * @return
     */
    public List<FilterRule> mockRule() {
        String mockStr = "[\n" +
                "  {\n" +
                "    \"entityFieldFullName\":\"com.amos.stuff.filter.bean.entity.BizEntity.name\",\n" +
                "    \"show\":false\n" +
                "  },\n" +
                "  {\n" +
                "    \"entityFieldFullName\":\"com.amos.stuff.filter.bean.entity.BizDetailEntity.experience\",\n" +
                "    \"show\":false\n" +
                "  }\n" +
                "]";
        List<FilterRule> list = JSONArray.parseArray(mockStr, FilterRule.class);
        return list;
    }
}
