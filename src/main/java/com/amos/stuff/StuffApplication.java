package com.amos.stuff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: StuffApplication
 * @Package: com.amos.stuff
 * @author: amos
 * @Description: 该项目主要是用于技能知识点的整理和记录
 *
 * 以不同的包来对应各种不同的知识点
 * common：整个 stuff 通用的代码模块
 *
 * @date: 2019/9/26 0026 上午 10:48
 * @Version: V1.0
 */
@SpringBootApplication
public class StuffApplication {

    public static void main(String[] args) {
        SpringApplication.run(StuffApplication.class, args);
    }

}
