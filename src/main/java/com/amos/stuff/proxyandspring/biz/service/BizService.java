package com.amos.stuff.proxyandspring.biz.service;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BizService
 * @Package: com.amos.stuff.proxyandspring.biz.service
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/27 0027 下午 14:13
 * @Version: V1.0
 */
public interface BizService {
    /**
     * 业务接口
     */
    void doSomething();
}
