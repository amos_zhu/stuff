package com.amos.stuff.proxyandspring.biz.impl;

import com.amos.stuff.proxyandspring.biz.AbstractCustomerPropertiesHandler;
import com.amos.stuff.proxyandspring.biz.type.CustomerPropertiesTypeEnum;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: CustomePropertiesArithmeticMultiSummationHandler
 * @Package: com.amos.stuff.proxyandspring.biz.impl
 * @author: zhuqb
 * @Description: 相乘后合计运算
 * @date: 2019/9/26 0026 下午 15:07
 * @Version: V1.0
 */
@Component
public class CustomePropertiesArithmeticMultiSummationHandler extends AbstractCustomerPropertiesHandler {


    @Override
    public CustomerPropertiesTypeEnum type() {
        return CustomerPropertiesTypeEnum.arithmeticMultiSummation;
    }

    @Override
    public String handler(Object data) {
        this.bizService.doSomething();
        this.logger.info("相乘后合计运算");
        return "相乘后合计运算";
    }
}
