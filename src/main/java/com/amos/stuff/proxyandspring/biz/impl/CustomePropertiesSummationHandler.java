package com.amos.stuff.proxyandspring.biz.impl;

import com.amos.stuff.proxyandspring.biz.AbstractCustomerPropertiesHandler;
import com.amos.stuff.proxyandspring.biz.type.CustomerPropertiesTypeEnum;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: CustomePropertiesSummationHandler
 * @Package: com.amos.stuff.proxyandspring.biz.impl
 * @author: zhuqb
 * @Description: 合计运算处理器
 * @date: 2019/9/26 0026 下午 14:52
 * @Version: V1.0
 */
@Component
public class CustomePropertiesSummationHandler extends AbstractCustomerPropertiesHandler {
    @Override
    public String handler(Object data) {
        this.bizService.doSomething();
        this.logger.info("合计运算");
        return "合计运算";
    }

    @Override
    public CustomerPropertiesTypeEnum type() {
        return CustomerPropertiesTypeEnum.summation;
    }
}
