package com.amos.stuff.proxyandspring.biz;

import com.amos.stuff.proxyandspring.biz.service.BizService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: AbstractCustomerPropertiesHandler
 * @Package: com.amos.stuff.proxyandspring.biz
 * @author: zhuqb
 * @Description: 通用的自定义属性运算处理
 * 新增这一层是为了拓展每个运算属性中通用的功能，减少代码量
 * @date: 2019/9/26 0026 上午 11:48
 * @Version: V1.0
 */
@Component
public abstract class AbstractCustomerPropertiesHandler implements CustomerPropertiesHandler {
    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected BizService bizService;
}
