package com.amos.stuff.proxyandspring.biz.type;

import com.amos.stuff.common.BaseEnum;
import com.amos.stuff.util.EnumFindHelper;
import lombok.AllArgsConstructor;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: CustomerPropertiesTypeEnum
 * @Package: com.amos.stuff.proxyandspring.biz.type
 * @author: zhuqb
 * @Description: 自定义属性类型枚举
 * 如果需要新增属性时，只需要在此枚举中新增对应的属性即可
 * @date: 2019/9/26 0026 下午 13:41
 * @Version: V1.0
 */
@AllArgsConstructor
public enum CustomerPropertiesTypeEnum implements BaseEnum {
    /**
     * 合计运算
     */
    summation("summation"),
    /**
     * 相乘后合计运算
     */
    arithmeticMultiSummation("arithmeticMultiSummation");
    String code;

    @Override
    public String getKey() {
        return this.code;
    }

    public static final EnumFindHelper<CustomerPropertiesTypeEnum, String> CODE_HELPER
            = new EnumFindHelper(CustomerPropertiesTypeEnum.class, new EnumFindHelper.EnumKeyGetter<CustomerPropertiesTypeEnum, String>() {

        @Override
        public String getKey(CustomerPropertiesTypeEnum enumValue) {
            return enumValue.code;
        }

    });

    public static CustomerPropertiesTypeEnum getByCode(String code) {
        return CODE_HELPER.find(code);
    }

}
