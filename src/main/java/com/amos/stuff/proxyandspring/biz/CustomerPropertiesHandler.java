package com.amos.stuff.proxyandspring.biz;

import com.amos.stuff.proxyandspring.biz.type.CustomerPropertiesTypeEnum;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: CustomerPropertiesHandler
 * @Package: com.amos.stuff.proxyandspring.biz
 * @author: zhuqb
 * @Description: 定义自定义属性处理的入口方法
 * 所有自定义属性处理器都需要实现该接口
 * @date: 2019/9/26 0026 下午 14:05
 * @Version: V1.0
 */
public interface CustomerPropertiesHandler {
    /**
     * 获取自定义属性的类型
     * 该还有种方法是可以自定义注解来指定自定义属性的类型
     * 方法是 在该接口的实现类中添加注解，注解中指定该自定义属性的类型
     * 接着在 CustomPropertiesTypeBeanInitialization 中通过反射获取所有含有该注解的类
     * 最后再组装对应的map数据（参考CustomPropertiesTypeBeanInitialization）
     *
     * @return 返回值直接使用自定义属性类型的枚举
     */
    CustomerPropertiesTypeEnum type();

    /**
     * 自定义属性处理
     * 实现类来处理对应的业务逻辑
     *
     * @param data 需要处理的数据
     * @return
     */
    String handler(Object data);
}
