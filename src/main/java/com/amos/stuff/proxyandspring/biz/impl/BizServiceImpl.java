package com.amos.stuff.proxyandspring.biz.impl;

import com.amos.stuff.proxyandspring.biz.service.BizService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: BizServiceImpl
 * @Package: com.amos.stuff.proxyandspring.biz.impl
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/27 0027 下午 14:14
 * @Version: V1.0
 */
@Service
public class BizServiceImpl implements BizService {
    private final static Logger logger = LoggerFactory.getLogger(BizServiceImpl.class);

    /**
     * 处理其他的事情
     */
    @Override
    public void doSomething() {
        logger.info("其他组件的业务处理");
    }
}
