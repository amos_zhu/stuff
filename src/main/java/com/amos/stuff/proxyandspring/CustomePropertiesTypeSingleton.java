package com.amos.stuff.proxyandspring;

import com.amos.stuff.proxyandspring.biz.CustomerPropertiesHandler;
import com.amos.stuff.proxyandspring.biz.type.CustomerPropertiesTypeEnum;
import com.amos.stuff.proxyandspring.biz.util.CustomPropertiesTypeBeanInitialization;

import java.util.Map;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: CustomePropertiesTypeSingleton
 * @Package: com.amos.stuff.proxyandspring
 * @author: zhuqb
 * @Description: 自定义属性类型单例
 * @date: 2019/9/26 0026 下午 15:19
 * @Version: V1.0
 */
public class CustomePropertiesTypeSingleton {

    private CustomePropertiesTypeSingleton() {
    }

    private static class Singleton {
        private static Map<CustomerPropertiesTypeEnum, CustomerPropertiesHandler> map;

        static {

            map = CustomPropertiesTypeBeanInitialization.getMap();
        }

        public static Map<CustomerPropertiesTypeEnum, CustomerPropertiesHandler> getInstance() {
            return map;
        }
    }

    public static Map<CustomerPropertiesTypeEnum, CustomerPropertiesHandler> getInstance() {
        return Singleton.getInstance();
    }
}
