package com.amos.stuff.proxyandspring.web;

import com.amos.stuff.common.BaseController;
import com.amos.stuff.common.Result;
import com.amos.stuff.common.ResultWapper;
import com.amos.stuff.proxyandspring.CustomePropertiesTypeSingleton;
import com.amos.stuff.proxyandspring.biz.type.CustomerPropertiesTypeEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: CustomPropertiesController
 * @Package: com.amos.stuff.proxyandspring.web
 * @author: zhuqb
 * @Description: 自定义属性Controller
 * @date: 2019/9/27 0027 上午 9:51
 * @Version: V1.0
 */
@RestController
@RequestMapping(value = "/custom-properties")
public class CustomPropertiesController extends BaseController {

    @GetMapping(value = "/handle/{handler}")
    public Result customProperties(@PathVariable("handler") CustomerPropertiesTypeEnum typeEnum) {
        this.logger.info(typeEnum.toString());
        return ResultWapper.success(CustomePropertiesTypeSingleton.getInstance().get(typeEnum).handler(null));
    }
}
