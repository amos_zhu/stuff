package com.amos.stuff.proxyandspring.runner;

import com.amos.stuff.proxyandspring.CustomePropertiesTypeSingleton;
import com.amos.stuff.proxyandspring.biz.CustomerPropertiesHandler;
import com.amos.stuff.proxyandspring.biz.type.CustomerPropertiesTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: stuff
 * @ClassName: CustomPropertiesTypeRunner
 * @Package: com.amos.stuff.proxyandspring.runner
 * @author: zhuqb
 * @Description:
 * @date: 2019/9/26 0026 下午 16:38
 * @Version: V1.0
 */
@Component
public class CustomPropertiesTypeRunner implements ApplicationRunner {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.logger.info("自定义属性类型初始化");
        Map<CustomerPropertiesTypeEnum, CustomerPropertiesHandler> map = CustomePropertiesTypeSingleton.getInstance();
        CustomerPropertiesHandler handler = map.get(CustomerPropertiesTypeEnum.arithmeticMultiSummation);
        handler.handler(null);
    }
}
